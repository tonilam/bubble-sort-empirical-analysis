Sorting is the computation of arranging a list of items into either ascending or descending order. Bubble sort uses two nested loops to compare and swapping the neighboring elements in order to obtain an ordered list at the end. As bubble sort compares each elements from the beginning to the end of a list, it is said to be a sequential sort. A more scientific definition as explained by Lewis and Chase (2004, p.126) is based on efficiency of the sorting algorithm. Typically, a sorting algorithm that involves a pair of nested loops requires roughly n2 comparisons to sort n elements is categorized to be a sequential sort. Therefore, bubble sort is categorized as a sequential sort.


The basic operation of the bubble sort algorithm involved the following steps:


1.	Scan through the list from the first element to the second last element


2.	Compare the current element with the next neighboring elements


3.	Depends on the outcome required is ascending or descending, if it is going to obtain the ascending ordered list, the two comparing elements will swap if and only if the next neighboring element is smaller than the current element; in contrast, the swapping will be taken place if and only if the next neighboring element is larger than the current element to obtain a descending ordered list.


4.	In ascending ordering case, the largest element in the list will be bubbled to the end of the list during the second layer loop (the inner loop).


5.	The iteration of the first layer loop (the outer loop) keeps going with the elimination of the bubbled up elements, i.e. on the second iteration of the first layer loop, the list size will become n-1.


6.	A better bubble sort algorithm is introduced in this analysis to prevent redundancy of keep iterating a completed sorted list before the nested loop end. It can be done by using a flag to check if the sorting is completed. The flag will start with a false value indicating the list is completely sorted, once there is a swapping taken place in the second layer loop, the flag is changed to true meaning that the list is not yet completely sorted. When the flag is false at the end of the second layer loop, the first layer loop will keep iterating, otherwise, the iteration will be terminated immediately.
