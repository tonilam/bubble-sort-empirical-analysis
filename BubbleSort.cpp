#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <ctime>

using namespace std;

const char *LOG_FILE_FORMAT = "samplegrowth%d.txt";
const int THIRTY_MIN = 60 * 30; // 60 sec x 30 min
const int SAMPLE_SIZE = 10;
const int SAMPLE_START = 1000;
const int SAMPLE_STEP = 1000;

long long unsigned int betterBubbleSort(int* arrayToSort, long int arraySize);
int inspectAlgorithm(long int problemSize, long long unsigned int *operationExecuted);
void generateRandomArray(int* randomArray, long int arraySize);
void printArray(string title, int* arrayToPrint, long int arraySize);
bool testSort(void);

int main() {
    ofstream mylog;
    int timeUsed = 0;
    int totalTimeUsed = 0;
    long long unsigned int operationExecuted = 0;
    char *logfile = new char[20];

    // uncomment this to test the functionality of betterBubbleSort:
    //cout << testSort();

    for (int sampleIndex = 0; sampleIndex < SAMPLE_SIZE; ++sampleIndex) {
        std::sprintf(logfile, LOG_FILE_FORMAT, sampleIndex);
        mylog.open(logfile);
        // Do as much as it can for one sample within 30 min:
        for (long int problemSize = SAMPLE_START; totalTimeUsed < THIRTY_MIN * (sampleIndex + 1); problemSize += SAMPLE_STEP) {
            timeUsed = inspectAlgorithm(problemSize, &operationExecuted);
            totalTimeUsed += timeUsed;
            cout << "SAMPLE[" << sampleIndex << "] ";
            cout << "Time used for A[" << problemSize << "]: " << timeUsed  << "s" << ", ";
            cout << "(Total time: " << totalTimeUsed << ")" << endl;
            mylog << problemSize << "\t" << operationExecuted << "\t" << timeUsed << endl;
        }

        mylog.close();
    }
    return 0;
}

long long unsigned int betterBubbleSort(int* arrayToSort, long int arraySize) {
    // The algorithm sorts arrayA[0..n-1] by improved bubble sort
    // Input: An array A[0..n-1] of orderable elements
    // Output: Array A[0..n-1] sorted in ascending order

    int counter = arraySize - 1; // number of adjacent pairs to be compared
    bool sflag = true; // swap flag
    long long unsigned int basicOperationCounter = 0;

    while (sflag) {
        sflag = false;
        for (int j = 0; j <= counter - 1; ++j) {
            if (arrayToSort[j+1] < arrayToSort[j]) {
                int temp = arrayToSort[j+1];
                arrayToSort[j+1] = arrayToSort[j];
                arrayToSort[j] = temp;
                sflag = true;
            }
            ++basicOperationCounter;
        }
        --counter;
    }
    return basicOperationCounter;
}

int inspectAlgorithm(long int problemSize, long long unsigned int *operationExecuted) {
        int arrayToProcess[problemSize] = {};

        generateRandomArray(arrayToProcess, problemSize);
        time_t before = time(0);
        *operationExecuted = betterBubbleSort(arrayToProcess, problemSize);
        time_t after = time(0);

        return after - before;
}

void generateRandomArray(int* randomArray, long int arraySize) {
    for (int i = 0; i < arraySize; ++i) {
        randomArray[i] = rand() % 10;
    }
}

bool testSort(void){
    int testingSize = 10;
    int testingArray1[testingSize] = {5, 1, 0, 9, 7, 8, 2, 3, 4, 6};
    int testingArray2[testingSize] = {5, 1, 8, 9, 7, 8, 2, 3, 4, 5};
    int expectedArray1[testingSize] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    int expectedArray2[testingSize] = {1, 2, 3, 4, 5, 5, 7, 8, 8, 9};
    betterBubbleSort(testingArray1, testingSize);
    betterBubbleSort(testingArray2, testingSize);
    bool result = true;
    for (int i = 0; i < testingSize; ++i){
        if (testingArray1[i] != expectedArray1[i]) {
            result = false;
            return false;
        }
    }
    for (int i = 0; i < testingSize; ++i){
        if (testingArray2[i] != expectedArray2[i]) {
            result = false;
            return false;
        }
    }
    return true;
}
